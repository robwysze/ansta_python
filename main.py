def zip_code_generate(var1, var2):
    zip_list = list()

    for code in range(int(var1.replace("-", "")), int(var2.replace("-", ""))):
        zip_list.append(str(code)[:2] + "-" + str(code)[2:])

    return zip_list


def check_list(to_check, n):
    missing = []
    for num in range(1, n+1):
        if num not in to_check:
            missing.append(num)

    return missing


if __name__ == "__main__":
    my_code = zip_code_generate("79-900", "80-155")
    missing_values = check_list([2, 3, 7, 4, 9], 10)
    # Task number 3
    my_number = [x*0.1 for x in range(20, 60, 5)]
